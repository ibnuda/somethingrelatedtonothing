package me.ijauradunbi.somethingrelatedtonothing.domain

import com.google.android.gms.maps.model.LatLng
import java.util.*

data class Kondisi(
        val now: Date = Date(),
        val latLng: LatLng,
        val ammo: Int,
        val health: Int
)

