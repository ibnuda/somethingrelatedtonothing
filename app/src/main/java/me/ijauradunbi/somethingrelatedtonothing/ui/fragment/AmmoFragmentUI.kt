package me.ijauradunbi.somethingrelatedtonothing.ui.fragment

import android.support.v4.app.Fragment
import android.view.View
import org.jetbrains.anko.*

class AmmoFragmentUI: AnkoComponent<Fragment> {
    override fun createView(ui: AnkoContext<Fragment>): View {
        return with(ui) {
            frameLayout {
                lparams(width = matchParent, height = wrapContent)
                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(20)
                    linearLayout {

                    }
                }
            }
        }
    }

}
